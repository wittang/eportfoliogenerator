/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

import static epg.StartupConstants.ICON_WINDOW_LOGO;
import static epg.StartupConstants.PATH_IMAGES;
import epg.file.EPortfolioExporter;
import epg.file.EPortfolioFileManager;
import epg.view.EPortfolioGeneratorView;
import java.io.File;
import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class EPortfolioGenerator extends Application{
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    EPortfolioFileManager fileManager = new EPortfolioFileManager();
    
    // THIS WILL EXPORT THE WEB SITES
    EPortfolioExporter ePortfolioExporter = new EPortfolioExporter();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    EPortfolioGeneratorView ui = new EPortfolioGeneratorView(fileManager, ePortfolioExporter);
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        String imagePath = PATH_IMAGES + ICON_WINDOW_LOGO;
	File file = new File(imagePath);
	
	// GET AND SET THE IMAGE
	URL fileURL = file.toURI().toURL();
	Image windowIcon = new Image(fileURL.toExternalForm());
	primaryStage.getIcons().add(windowIcon);
        String appTitle = "ePortfolio Generator";
        ui.startUI(primaryStage, appTitle);
}
     public static void main(String[] args) {
	launch(args);
    }
}
