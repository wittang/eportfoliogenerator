/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

/**
 *
 * @author Wilson
 */
public class StartupConstants {
    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS
    public static String PATH_DATA = "./data/";
    public static String PATH_EPORTFOLIOS = PATH_DATA + "ePortfolios/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "/epg/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioGeneratorStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "EPG_Logo.png";
    public static String ICON_NEW_EPORTFOLIO = "New.png";
    public static String ICON_LOAD_EPORTFOLIO = "Load.png";
    public static String ICON_SAVE_EPORTFOLIO = "Save.png";
    public static String ICON_EXPORT_EPORTFOLIO = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD = "Add.png";
    public static String ICON_REMOVE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_EDIT = "Edit.png";

    // UI SETTINGS
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    
    // CSS - FOR THE LANGUAGE SELECTION DIALOG
    public static String    CSS_CLASS_LANG_DIALOG_PANE = "dialogpane";
    public static String    CSS_CLASS_LANG_PROMPT = "lang_prompt";
    public static String    CSS_CLASS_LANG_COMBO_BOX = "lang_combo_box";
    public static String    CSS_CLASS_LANG_OK_BUTTON = "lang_ok_button";

    // CSS - FOR THE TOOLBARS
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_PANE = "horizontal_toolbar_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_PANE = "vertical_toolbar_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";

    // CSS - SLIDESHOW EDITING
    public static String    CSS_CLASS_TITLE_PANE = "title_pane";
    public static String    CSS_CLASS_TITLE_PROMPT = "title_prompt";
    public static String    CSS_CLASS_TITLE_TEXT_FIELD = "title_text_field";
    public static String    CSS_CLASS_CAPTION_PROMPT = "caption_prompt";
    public static String    CSS_CLASS_CAPTION_TEXT_FIELD = "caption_text_field";
    
    // CSS - PANELS
    public static String    CSS_CLASS_WHOLESPACE = "wholespace";
    public static String    CSS_CLASS_PAGE_VIEW = "pageview";
    public static String    CSS_CLASS_WORKSPACE = "workspace";
    public static String    CSS_CLASS_SLIDES_EDITOR_PANE = "slides_editor_pane";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW = "selected_slide_edit_view";
    public static String    CSS_CLASS_PAGE_EDIT_HBOX = "pageEditHBox";
    public static String    CSS_CLASS_WORK_SPACE_MODE = "workspacemode";
    public static String    CSS_CLASS_PAGES = "pages";
    public static String    CSS_CLASS_PAGE_EDIT_VBOX = "pageEditVBox";
    public static String    CSS_CLASS_PAGE_EDIT_TOOLBAR = "pageEditToolbar";
    public static String    CSS_CLASS_COMPONENT_VIEW = "componentview";
    public static String    CSS_CLASS_SELECTED_ITEM = "selected_item";
    public static String    CSS_CLASS_SCROLLPANE = "scrollpane";

    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_SELECTION_PROMPT = "Select a Language:";
    public static String    OK_BUTTON_TEXT = "OK";
}
