/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.model.Page;
import epg.view.EPortfolioGeneratorView;
import epg.view.ImageDialogView;
import epg.view.PageEditView;
import epg.view.SlideshowDialogView;
import epg.view.TextDialogView;
import epg.view.VideoDialogView;

/**
 *
 * @author Wilson
 */
public class PageEditController {
    EPortfolioGeneratorView ui;
    PageEditView pageEditView;
    Page page;
    String editComponentType;
    public PageEditController(EPortfolioGeneratorView initUI, PageEditView initPageEditView, Page initPage){
        this.ui = initUI;
        this.pageEditView = initPageEditView;
        this.page = initPage;
    }
    public void processAddComponentRequest(String componentType){
        if (componentType.equals("Text")){
            TextDialogView textDialogView = new TextDialogView(ui.getWindow(), page, false);
            textDialogView.show("Add Text Component");
        }
        if (componentType.equals("Image")){
            ImageDialogView imageDialogView = new ImageDialogView(ui.getWindow(), page, false);
            imageDialogView.show("Add Image Component");
        }
        if (componentType.equals("Video")){
            VideoDialogView videoDialogView = new VideoDialogView(ui.getWindow(),page, false);
            videoDialogView.show("Add Video Component");
        }
        if (componentType.equals("Slideshow")){
            SlideshowDialogView slideshowDialogView = new SlideshowDialogView(ui.getWindow(),page,false);
            slideshowDialogView.show("Add Slideshow Component");
        }
        pageEditView.reloadComponentsPane();
    }
    public void processRemoveComponentRequest(){
	page.removeSelectedComponent();
	pageEditView.reloadComponentsPane();
    }
    public void processEditComponentRequest(){
        editComponentType = page.getSelectedComponent().getComponentType();
        if (editComponentType.equals("Paragraph") || editComponentType.equals("Header")){
            TextDialogView textDialogView = new TextDialogView(ui.getWindow(), page, true);
            textDialogView.show("Edit " + editComponentType);
        }
        if (editComponentType.equals("List")){
            TextDialogView textDialogView = new TextDialogView(ui.getWindow(), page, true);
            textDialogView.show("Edit " + editComponentType);
        }
        if (editComponentType.equals("Image")){
            ImageDialogView imageDialogView = new ImageDialogView(ui.getWindow(), page, true);
            imageDialogView.show(" Edit Image Component");
        }
        if (editComponentType.equals("Video")){
            VideoDialogView videoDialogView = new VideoDialogView(ui.getWindow(), page, true);
            videoDialogView.show("Edit Video Component");
        }
        if (editComponentType.equals("Slideshow")){
            SlideshowDialogView slideshowDialogView = new SlideshowDialogView(ui.getWindow(), page, true);
            slideshowDialogView.show("Edit Slideshow Component");
        }
        pageEditView.reloadComponentsPane();
        System.out.println(page.getComponents());
    }
    
}
