/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.model.EPortfolio;
import epg.model.Page;
import epg.view.EPortfolioGeneratorView;

/**
 *
 * @author Wilson
 */
public class EPortfolioController {
    private EPortfolioGeneratorView ui;
    Page page;

    public EPortfolioController(EPortfolioGeneratorView initUI) {
        ui = initUI;
    }
    public void processAddPageRequest() {
	EPortfolio ePortfolio = ui.getEPortfolio();
	ePortfolio.addPage();
        ui.reloadPagePane();
	//ui.updateFileToolbarControls(false);
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to remove it from the slide show.
     */
    public void processRemovePageRequest() {
	EPortfolio ePortfolio = ui.getEPortfolio();
	ePortfolio.removeSelectedPage();
	ui.reloadPagePane();
	ui.updateFileToolbarControls(false);
    }
}
