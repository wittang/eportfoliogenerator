package epg.controller;

import static epg.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static epg.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import epg.model.SlideshowComponent;
import epg.view.SlideshowDialogView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowEditController {
    // APP UI
    private SlideshowDialogView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideshowDialogView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideshowComponent slideShow = ui.getSlideshow();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES, "");
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to remove it from the slide show.
     */
    public void processRemoveSlideRequest() {
	SlideshowComponent slideShow = ui.getSlideshow();
	slideShow.removeSelectedSlide();
	ui.reloadSlideShowPane();
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to move it up in the slide show.
     */
    public void processMoveSlideUpRequest() {
	SlideshowComponent slideShow = ui.getSlideshow();
	slideShow.moveSelectedSlideUp();	
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wises to move it down in the slide show.
     */
    public void processMoveSlideDownRequest() {
	SlideshowComponent slideShow = ui.getSlideshow();
	slideShow.moveSelectedSlideDown();
    }
}
