/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import static epg.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static epg.StartupConstants.PATH_IMAGES;
import static epg.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import java.io.File;
import javafx.stage.FileChooser;

/**
 *
 * @author Wilson
 */
public class VideoSelectionController {
       
    /**
     * Default contstructor doesn't need to initialize anything
     */
    public VideoSelectionController() {   
    }
    
    public String processSelectVideo(String path) {
	FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	imageFileChooser.setInitialDirectory(new File(PATH_IMAGES));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.MP4");
	imageFileChooser.getExtensionFilters().add(mp4Filter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    return file.getPath();//.substring(0, file.getPath().indexOf(file.getName()));
	    /*pageToEdit.setImage(path, fileName);
	    view.updateSlideImage();
	    ui.updateFileToolbarControls(false);*/
	}
        return path;
    }
}
