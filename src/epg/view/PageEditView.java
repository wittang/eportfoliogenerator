/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.*;
import epg.controller.ImageSelectionController;
import epg.controller.PageEditController;
import epg.error.ErrorHandler;
import epg.model.Component;
import epg.model.Page;
import java.io.File;
import java.net.URL;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Wilson
 */
public class PageEditView extends HBox{
    EPortfolioGeneratorView ui;
    
    // SLIDE THIS COMPONENT EDITS
    Page page;
    
    // VBOX for editing Page
    VBox pageEditVBox;
    HBox pageTitleHBox;
    Label pageTitle;
    TextField titleTextField;
    //HBox pageDisplay;
    ComboBox layout;
    ComboBox color;
    ComboBox font;
    HBox footerHBox;
    Label footer;
    TextField footerTextField;
    Label bannerImage;
    // DISPLAYS THE IMAGE FOR THE BANNER
    ImageView imageSelectionView;
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    String path;
    
    Label components;
    ScrollPane componentsScrollPane;
    VBox componentsPane;
    ComponentView componentView;
    
    //VBOX for pageEditToolBar
    PageEditController pageEditController;
    VBox pageEditToolbar;
    ComboBox componentSelection;
    Button addComponentButton;
    Button removeComponentButton;
    Button editComponentButton; 
    String componentChoice;
    

    public PageEditView(EPortfolioGeneratorView initUi, Page initPage) {
    // KEEP THIS FOR LATER
	ui = initUi;
	
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	//this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	page = initPage;
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_HBOX);
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
        path = page.getBannerImagePath();
	updateBannerImage(path);
        
	// SETUP THE PAGE EDIT OPTIONS
        pageEditVBox = new VBox(10);
        
        //Page Title HBox and panes
        pageTitleHBox = new HBox();
        pageTitleHBox.getStyleClass().add(CSS_CLASS_PAGE_EDIT_HBOX);
        pageTitle = new Label("Page Title: ");
        pageTitle.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        titleTextField = new TextField();
        titleTextField.setText(page.getTitle());
        pageTitleHBox.getChildren().addAll(pageTitle, titleTextField);
        
        
        //Page display selection HBox
        
        GridPane pageDisplay = new GridPane();
        Label layoutLabel = new Label("Font");
        layoutLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        Label colorLabel = new Label("Color");
        colorLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        Label fontLabel = new Label("Font");
        fontLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        pageDisplay.setAlignment(Pos.CENTER);
        ColumnConstraints column0 = new ColumnConstraints();
        column0.setHalignment(HPos.CENTER);
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHalignment(HPos.CENTER);
        pageDisplay.getColumnConstraints().addAll(column0, column1, column2);
        layoutLabel.setTextAlignment(TextAlignment.CENTER);
        colorLabel.setTextAlignment(TextAlignment.CENTER);
        fontLabel.setTextAlignment(TextAlignment.CENTER);
        ObservableList<String> layoutChoices = FXCollections.observableArrayList();
        ObservableList<String> colorChoices = FXCollections.observableArrayList();
        ObservableList<String> fontChoices = FXCollections.observableArrayList();
	layoutChoices.addAll("Layout 1", "Layout 2", "Layout 3", "Layout 4", "Layout 5");
	layout = new ComboBox(layoutChoices);
	colorChoices.addAll("Sky Blue/Grey", "Wheat/Silver", "Nav/Dark Green", "Plum/Seashell", "Sea Green/Teal");
	color = new ComboBox(colorChoices);
        color.getSelectionModel().select("Color 1");
        fontChoices.addAll("Bitter", "Josefin Sans", "Abril Fatface", "Architects Daughter", "Permanent Marker");
        font = new ComboBox(fontChoices);
        font.getSelectionModel().select("Font 1");
        //pageDisplay.getChildren().addAll(layout, color, font);
        layout.getSelectionModel().select(page.getLayout());
        color.getSelectionModel().select(page.getColor());
        font.getSelectionModel().select(page.getFont());
        layout.setOnAction(e->{
            page.setLayout(layout.getValue().toString());
            ui.updateFileToolbarControls(false);
        });
        color.setOnAction(e->{
            page.setColor(color.getValue().toString());
            ui.updateFileToolbarControls(false);
        });
        font.setOnAction(e->{
            page.setFont(font.getValue().toString());
            ui.updateFileToolbarControls(false);
        });
        layout.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        color.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        font.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        pageDisplay.add(layoutLabel, 0, 0);
        pageDisplay.add(colorLabel, 1, 0);
        pageDisplay.add(fontLabel, 2, 0);
        pageDisplay.add(layout, 0, 1);
        pageDisplay.add(color, 1, 1);
        pageDisplay.add(font, 2, 1);
        pageDisplay.setHgap(50);
        //Page Footer HBox and panes
        footerHBox = new HBox();
        footerHBox.getStyleClass().add(CSS_CLASS_PAGE_EDIT_HBOX);
        footerHBox.setAlignment(Pos.CENTER);
        footer = new Label("Footer: ");
        footer.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        footerTextField = new TextField();
        footerTextField.setText(page.getFooter());
        footerTextField.setMinWidth(300);
        footerHBox.getChildren().addAll(footer, footerTextField);
        
        bannerImage = new Label("Banner Image:");
        bannerImage.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        components = new Label("Components:");
        components.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        componentsPane = new VBox(10);
        componentsPane.setMaxWidth(Double.MAX_VALUE);
        componentsPane.setAlignment(Pos.CENTER);
        componentsScrollPane = new ScrollPane(componentsPane);
        componentsPane.getStyleClass().add(CSS_CLASS_PAGES);
        componentsScrollPane.getStyleClass().add(CSS_CLASS_PAGES);
        pageEditVBox.getChildren().addAll(pageTitleHBox, pageDisplay, bannerImage, imageSelectionView, footerHBox, components, componentsScrollPane);
        pageEditVBox.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VBOX);
        pageEditToolbar = new VBox(10);
        ObservableList<String> componentChoices = FXCollections.observableArrayList();
	componentChoices.addAll("Text", "Image", "Video", "Slideshow");
        componentSelection = new ComboBox(componentChoices);
        componentSelection.getSelectionModel().select("Text");
        componentChoice = componentSelection.getValue().toString();
        componentSelection.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        pageEditToolbar.getChildren().add(componentSelection);
        addComponentButton = this.initChildButton(pageEditToolbar, ICON_ADD, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
	removeComponentButton = this.initChildButton(pageEditToolbar, ICON_REMOVE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        editComponentButton = this.initChildButton(pageEditToolbar, ICON_EDIT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        
        initEventHandlers();
        reloadComponentsPane();
        
        getChildren().addAll(pageEditVBox, pageEditToolbar);
        setHgrow(pageEditVBox, Priority.ALWAYS);
        pageEditToolbar.getStyleClass().add(CSS_CLASS_PAGE_EDIT_TOOLBAR);
        
        
        

	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	    path = imageController.processSelectImage(path);
            updateBannerImage(path);
	});
	/*captionTextField.textProperty().addListener(e -> {
	    String text = captionTextField.getText();
	    slide.setCaption(text);	 
	    ui.updateFileToolbarControls(false);
	});
	
	// CHOOSE THE STYLE
	captionLabel.getStyleClass().add(CSS_CLASS_CAPTION_PROMPT);
	captionTextField.getStyleClass().add(CSS_CLASS_CAPTION_TEXT_FIELD);
        */
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
    public void initEventHandlers(){
        titleTextField.textProperty().addListener(e -> {
	    page.setTitle(titleTextField.getText());
	    ui.updateFileToolbarControls(false);
            ui.reloadPagePane();
	});
        footerTextField.textProperty().addListener(e -> {
	    page.setFooter(footerTextField.getText());
	    ui.updateFileToolbarControls(false);
	});
        componentSelection.setOnAction(e->{
            componentChoice = componentSelection.getValue().toString();
        }
        );
        pageEditController = new PageEditController(ui,this, page);
        addComponentButton.setOnAction(e->{
            pageEditController.processAddComponentRequest(componentChoice);
        });
        removeComponentButton.setOnAction(e->{
            pageEditController.processRemoveComponentRequest();
        });
        editComponentButton.setOnAction(e->{
            pageEditController.processEditComponentRequest();
        });
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateBannerImage(String path) {
	File file = new File(path);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = 800;
	    double perc = scaledWidth / (4*slideImage.getWidth());
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
            page.setBannerImageName(file.getName());
            page.setBannerImagePath(path);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError();
	}
    }   
    public void reloadComponentsPane(){
        componentsPane.getChildren().clear();
	for (Component component : page.getComponents()) {
	    ComponentView componentView = new ComponentView(this, component);
	    if (page.isSelectedComponent(component))
		componentView.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
	    else
		componentView.getStyleClass().add(CSS_CLASS_PAGE_VIEW);
	    componentsPane.getChildren().add(componentView);
	    componentView.setOnMousePressed(e -> {
		page.setSelectedComponent(component);
		this.reloadComponentsPane();
	    });
	}
	updatePageEditToolbarControls();
    }
    public void updatePageEditToolbarControls() {
	// AND THE SLIDESHOW EDIT TOOLBAR
	addComponentButton.setDisable(false);
	boolean componentSelected = page.isComponentSelected();
	removeComponentButton.setDisable(!componentSelected);
	editComponentButton.setDisable(!componentSelected);	
    }
}


