/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.CSS_CLASS_PAGES;
import static epg.StartupConstants.CSS_CLASS_SCROLLPANE;
import static epg.StartupConstants.CSS_CLASS_SELECTED_ITEM;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_ADD;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import epg.model.HyperlinkComponent;
import epg.model.ListComponent;
import epg.model.Page;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class ListDialogView extends VBox{
    //VBox listPane;
    Scene listScene;
    Page page;
    Label listLabel;
    ScrollPane listScrollPane;
    VBox listVBox;
    //TextField textField;
    Button addItemButton;
    Button removeItemButton;
    Button okButton;
    Button cancelButton;
    ListComponent tempList;
    Button addHyperlinkButton;
    TextArea fieldWithLink;
    int itemNo;
    //ObservableList<TextField> textFields;
    //Node selectedItem;
    int selectedItemIndex;
    HyperlinkList hyperlinks;
    HyperlinkDialogView hyperlinkDialog;
    //TextField currentTextField;
    
    public ListDialogView(Scene initScene, Page initPage, boolean editPressed) {
        listScene = initScene;
        page = initPage;
        tempList = new ListComponent();
        //tempList.getItems().add("");
        //textFields = FXCollections.observableArrayList();
        listLabel = new Label("Add List");
        if (editPressed){
            listLabel.setText("Edit List");
            tempList = (ListComponent)page.getSelectedComponent();
        }
        //textField = new TextField();
        //currentTextField = textField;
        //Label star = new Label("*");
        listVBox = new VBox();
        addHyperlinkButton = new Button("Add Hyperlink");
        addHyperlinkButton.setDisable(true);
        hyperlinks = new HyperlinkList((Stage)listScene.getWindow(), tempList);
        //paragraphHBox.setHgrow(hyperlinks, Priority.ALWAYS);
        addHyperlinkButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        addHyperlinkButton.setOnMouseClicked(e->{
            System.out.println(fieldWithLink.getSelectedText());
            if (!fieldWithLink.getSelectedText().isEmpty()){
                hyperlinkDialog = new HyperlinkDialogView((Stage)listScene.getWindow(), "");
                HyperlinkComponent hyperlink = new HyperlinkComponent(fieldWithLink.getSelectedText(), 
                    hyperlinkDialog.hyperlink, 
                    fieldWithLink.getSelection().getStart(), 
                    fieldWithLink.getSelection().getEnd(), 
                    itemNo);
                if (hyperlinkDialog.choice.equals("add"))
                    tempList.getHyperlinks().add(hyperlink);
            System.out.println(hyperlink.getText() + ":" + hyperlink.getUrl() + ":" +hyperlink.getStart() + "-" +hyperlink.getEnd() +" itemNo " + hyperlink.getItemNo());
            addHyperlinkButton.setDisable(true);
            }
            hyperlinks.reloadItemsPane();
        });
        listScrollPane = new ScrollPane(listVBox);
        listScrollPane.setFitToHeight(true);
        listScrollPane.setFitToWidth(true);
        VBox addRemoveToolbar = new VBox();
        listScrollPane.getStyleClass().add(CSS_CLASS_SCROLLPANE);
        addItemButton = initChildButton(addRemoveToolbar, ICON_ADD, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        removeItemButton = initChildButton(addRemoveToolbar, ICON_REMOVE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        initHandlers();
        HBox listHBox = new HBox();
        listHBox.getChildren().addAll(listScrollPane, addRemoveToolbar);
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
        listLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        getChildren().addAll(listLabel, listHBox, addHyperlinkButton, hyperlinks, buttonBox);
        
        //currentTextField = textField;
        okButton.setOnAction(e->{
            //for (int i = 0; i<tempList.getItems().size(); i++)
              //  tempList.getItems().set(i, ((TextField)((HBox)listVBox.getChildren().get(i)).getChildren().get(1)).getText());
            if (editPressed)
                page.getComponents().set(page.getComponents().indexOf(page.getSelectedComponent()), tempList);
            else
                page.addComponent(tempList);
            ((Stage)listScene.getWindow()).close();
        });
        cancelButton.setOnAction(e->{
                ((Stage)listScene.getWindow()).close();
        });
        listHBox.setHgrow(listScrollPane, Priority.ALWAYS);
        listScene.setRoot(this);
        selectedItemIndex = -1;
        reloadItemsPane();
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath) {};
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
    /*
    public void initHandlers(){
         addItemButton.setOnMouseClicked(e->{
            Label star1 = new Label(" *");
            star1.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
            HBox item1 = new HBox();
            textField = new TextField();
            item1.getChildren().addAll(star1, textField);
            listVBox = (VBox)listScrollPane.getContent();
            tempList.getItems().add("");
            listVBox.getChildren().add(item1);
            reloadItemsPane();
        });
        removeItemButton.setOnMouseClicked(e->{
            listVBox = (VBox)listScrollPane.getContent();
            if (tempList.getItems().size() > 1){
                tempList.getItems().remove(selectedItemIndex);
                listVBox.getChildren().remove(selectedItemIndex);
                selectedItem = null;
                reloadItemsPane();
            }
        });
    }
    
    public boolean isSelectedComponent(Node testItem) {
	return selectedItem == testItem;
    }
    
    public void reloadItemsPane(){
        VBox tempListVBox = new VBox();
        textFields.clear();
        listVBox = (VBox)listScrollPane.getContent();
	for (Node vBoxNode : listVBox.getChildren()) {
            int index = listVBox.getChildren().indexOf(vBoxNode);
            String text = tempList.getItems().get(index);
            Label star1 = new Label(" *");
            star1.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
            HBox item1 = new HBox();
            textField = new TextField();
            textField.setText(text);
            textFields.add(textField);
            textField.setOnMouseClicked(e->{
                currentTextField = textFields.get(index);
            });
            textField.setOnKeyReleased(e->{
                tempList.getItems().set(index, currentTextField.getText());
            });
            item1.getChildren().addAll(star1, textField);
	    if (isSelectedComponent(vBoxNode)){
		item1.getStyleClass().add(CSS_CLASS_SELECTED_ITEM);
            }
            else{
                item1.getStyleClass().remove(CSS_CLASS_SELECTED_ITEM);
            }
	    tempListVBox.getChildren().add(item1);
	    item1.setOnMousePressed(e -> {
                removeItemButton.setDisable(false);
                selectedItemIndex = index;
		selectedItem = item1;
		reloadItemsPane();
	    });
            listScrollPane.setContent(tempListVBox);
            tempListVBox.setPadding(new Insets(0, 30, 0, 30));
	}
    } 
    */
    public void initHandlers(){
        addItemButton.setOnMouseClicked(e->{
            tempList.getItems().add("");
            System.out.println(tempList.getItems());
            reloadItemsPane();
        });
        removeItemButton.setOnMouseClicked(e->{
            for (int i = 0; i<tempList.getHyperlinks().size(); i++){
                System.out.println(i);
                if(tempList.getHyperlinks().get(i).getItemNo()==itemNo){
                    tempList.getHyperlinks().remove(i);
                    i--;
                    System.out.println(i);
                }
            }
            tempList.getItems().remove(selectedItemIndex);
            hyperlinks.reloadItemsPane();
            selectedItemIndex = -1;
            reloadItemsPane();
            
        });
    }
    public void reloadItemsPane(){
        listVBox.getChildren().clear();
        if (selectedItemIndex < 0){
            removeItemButton.setDisable(true);
        }
	for (int i = 0; i<tempList.getItems().size(); i++) {
            int index = i;
            System.out.println("item " +index+ " is being loaded onto list");
            Label star = new Label("  *");
            star.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
            HBox itemHBox = new HBox();
            TextArea textField = new TextArea();
            textField.setMaxWidth(175);
            textField.setMaxHeight(10);
            itemHBox.getChildren().addAll(star, textField);
            textField.setText(tempList.getItems().get(index));
            textField.textProperty().addListener(e->{
                tempList.getItems().set(index, textField.getText());
            });
            textField.setOnDragDetected(e->{
                if(!textField.getSelectedText().isEmpty()){
                    addHyperlinkButton.setDisable(false);
                    fieldWithLink = textField;
                    itemNo = index;
                }
            });
            if (selectedItemIndex == index){
                System.out.println("this hyper link is selected");
                itemHBox.getStyleClass().add(CSS_CLASS_SELECTED_ITEM);
            }
            else{
                itemHBox.getStyleClass().remove(CSS_CLASS_SELECTED_ITEM);
            }
            listVBox.getChildren().add(itemHBox);
            itemHBox.setOnMousePressed(e->{
                itemNo = index;
                removeItemButton.setDisable(false);
                selectedItemIndex = index;
                System.out.println("item " + selectedItemIndex + " is selected");
                reloadItemsPane();
            });
        }
    } 
}
