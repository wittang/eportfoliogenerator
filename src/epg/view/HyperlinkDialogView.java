/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class HyperlinkDialogView extends Stage {
    VBox textPane;
    ComboBox textChoice;
    Scene textScene;
    Label textLabel;
    Label urlLabel;
    TextField urlTextField;
    HBox urlText;
    String hyperlink;
    Button okButton;
    Button cancelButton;
    String choice;
    public HyperlinkDialogView(Stage primaryStage, String url) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        textLabel = new Label("Hyperlink Component"); 
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        // NOW ORGANIZE OUR BUTTONS
        urlLabel = new Label("URL:");
        urlTextField = new TextField();
        urlTextField.setText(url);
        urlText = new HBox();
        urlText.getChildren().addAll(urlLabel, urlTextField);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
	
        // WE'LL PUT EVERYTHING HERE
        textPane = new VBox();
        textLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        urlLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        urlText.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getChildren().addAll(textLabel, urlText, buttonBox);

	// CSS CLASSES
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        // MAKE IT LOOK NICE

        // AND PUT IT IN THE WINDOW
        textScene = new Scene(textPane);
	textScene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(textScene);
        okButton.setOnAction(e->{
                hyperlink = urlTextField.getText();
                choice = "add";
                close();
        });
        cancelButton.setOnAction(e->{
            choice = "";
                close();
        });
        showAndWait();
    }
    public String getHyperlink(){
        return hyperlink;
    }
}
