/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_SLIDES_EDITOR_PANE;
import static epg.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static epg.StartupConstants.CSS_CLASS_TITLE_PANE;
import static epg.StartupConstants.CSS_CLASS_TITLE_PROMPT;
import static epg.StartupConstants.CSS_CLASS_TITLE_TEXT_FIELD;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_PANE;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE;
import static epg.StartupConstants.ICON_ADD;
import static epg.StartupConstants.ICON_MOVE_DOWN;
import static epg.StartupConstants.ICON_MOVE_UP;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.controller.SlideShowEditController;
import epg.error.ErrorHandler;
import epg.model.Page;
import epg.model.Slide;
import epg.model.SlideshowComponent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class SlideshowDialogView extends Stage{
    
    // WORKSPACE
    HBox workspace;
    VBox slideshowPane;
    Scene slideshowScene;
    Label slideshowLabel;
    Button okButton;
    Button cancelButton;
    int zero;
    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    FlowPane slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
    
    // FOR THE SLIDE SHOW TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideshowComponent slideshow;
    Page page;
    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    //SlideShowFileManager fileManager;
    
    // THIS IS FOR EXPORTING THE SLIDESHOW SITE
    //SlideShowSiteExporter siteExporter;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    //private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;
    public SlideshowDialogView(Stage primaryStage, Page initPage, boolean editPressed) {
        page = initPage;
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        slideshow = new SlideshowComponent();
        slideshow.setUI(this);
        if (editPressed){
            SlideshowComponent tempSlideshow = (SlideshowComponent)page.getSelectedComponent();
            slideshow.setTitle(tempSlideshow.getTitle());
            for (int i=0; i<tempSlideshow.getSlides().size(); i++){
                slideshow.addSlide(tempSlideshow.getSlides().get(i).getImageFileName(),
                        tempSlideshow.getSlides().get(i).getImagePath(),
                        tempSlideshow.getSlides().get(i).getCaption());
                }
        }
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
        slideshowLabel = new Label();
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new FlowPane();
	addSlideButton = this.initChildButton(slideEditToolbar, ICON_ADD, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	moveSlideUpButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	moveSlideDownButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
          
        initEventHandlers();
	
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
	slidesEditorScrollPane.setFitToWidth(true);
	slidesEditorScrollPane.setFitToHeight(true);
	initTitleControls();
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().addAll(slideEditToolbar, slidesEditorScrollPane);

	// SETUP ALL THE STYLE CLASSES
	workspace.getStyleClass().add(CSS_CLASS_WORKSPACE);
	slideEditToolbar.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
	slidesEditorPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
	slidesEditorScrollPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
        
        slideshowPane = new VBox();
        slideshowLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        slideshowPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        slideshowPane.getChildren().addAll(slideshowLabel, workspace, buttonBox);
        okButton.setOnMousePressed(e->{
            if (!editPressed)
                page.addComponent(slideshow);
            else 
                page.getComponents().set(page.getComponents().indexOf(page.getSelectedComponent()), slideshow);
            close();
        });
        cancelButton.setOnMousePressed(e->{
            close();
        });
        // AND PUT IT IN THE WINDOW
        slideshowScene = new Scene(slideshowPane);
	slideshowScene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(slideshowScene);
        reloadSlideShowPane();
    }
    public void updateSlideshowEditToolbarControls() {
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
	boolean slideSelected = slideshow.isSlideSelected();
	removeSlideButton.setDisable(!slideSelected);
	moveSlideUpButton.setDisable(!slideSelected);
	moveSlideDownButton.setDisable(!slideSelected);	
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane() {
	slidesEditorPane.getChildren().clear();
	reloadTitleControls();
	for (Slide slide : slideshow.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(this, slide);
	    if (slideshow.isSelectedSlide(slide))
		slideEditor.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
	    else
		slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	    slidesEditorPane.getChildren().add(slideEditor);
	    slideEditor.setOnMousePressed(e -> {
		slideshow.setSelectedSlide(slide);
		this.reloadSlideShowPane();
	    });
	}
	updateSlideshowEditToolbarControls();
    }
    private void initEventHandlers(){
        editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
            reloadSlideShowPane();
            if(zero < 2)
                slideshowScene.getWindow().sizeToScene();
            zero++;
	});
	removeSlideButton.setOnAction(e -> {
	    editController.processRemoveSlideRequest();
	});
	moveSlideUpButton.setOnAction(e -> {
	    editController.processMoveSlideUpRequest();
	});
	moveSlideDownButton.setOnAction(e -> {
	    editController.processMoveSlideDownRequest();
	});
    }
    
    private void initTitleControls() {
	titlePane = new FlowPane();
	titleLabel = new Label("Slideshow Title: ");
	titleTextField = new TextField();
	
	titlePane.getChildren().add(titleLabel);
	titlePane.getChildren().add(titleTextField);
	
	titleTextField.textProperty().addListener(e -> {
	    slideshow.setTitle(titleTextField.getText());
	});
	
	titlePane.getStyleClass().add(CSS_CLASS_TITLE_PANE);
	titleLabel.getStyleClass().add(CSS_CLASS_TITLE_PROMPT);
	titleTextField.getStyleClass().add(CSS_CLASS_TITLE_TEXT_FIELD);
    }
    
    public void reloadTitleControls() {
	if (slidesEditorPane.getChildren().size() == 0)
	    slidesEditorPane.getChildren().add(titlePane);
	titleTextField.setText(slideshow.getTitle());
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath) {};
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
    public void show(String message) {
        slideshowLabel.setText(message);
        this.showAndWait();
    }
    public SlideshowComponent getSlideshow(){
        return slideshow;
    }
}

