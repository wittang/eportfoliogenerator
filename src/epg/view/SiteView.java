/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;
import epg.model.EPortfolio;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author Wilson
 */
public class SiteView extends VBox {
    
    // THE MAIN UI

    EPortfolioGeneratorView parentView;

    // THE DATA FOR THIS SLIDE SHOW
    EPortfolio ePortfolio;

    // HERE ARE OUR UI CONTROLS
    ScrollPane scrollPane;
    WebView webView;
    WebEngine webEngine;

    /**
     * This constructor just initializes the parent and slides references, note
     * that it does not arrange the UI or start the slide show view window.
     *
     * @param initParentView Reference to the main UI.
     */
    public SiteView(EPortfolioGeneratorView initParentView){
	// KEEP THIS FOR LATER
	parentView = initParentView;

	// SETUP THE UI
	webView = new WebView();
        this.setAlignment(Pos.CENTER);
	
	// GET THE URL
	//String indexPath = SITES_DIR + slides.getTitle() + SLASH + INDEX_FILE;
        String indexPath = "./sites/index.html";
	File indexFile = new File(indexPath);
        try {
            URL indexURL = indexFile.toURI().toURL();
        
	
	// SETUP THE WEB ENGINE AND LOAD THE URL
            webEngine = webView.getEngine();
            webEngine.load(indexURL.toExternalForm());
            webEngine.setJavaScriptEnabled(true);
	
	// SET THE WINDOW TITLE
	//this.setTitle(slides.getTitle());

	// NOW PUT STUFF IN THE STAGE'S SCENE
            this.getChildren().add(webView);
            //this.setContent(webView);
            //this.getStyleClass().add(CSS_CLASS_PAGES);
            //webView.getStyleClass().add(CSS_CLASS_PAGES);
        } 
        catch (MalformedURLException e){
            System.out.println("URL error");
        }
    }
}
