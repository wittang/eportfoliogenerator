/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.controller.VideoSelectionController;
import epg.model.Page;
import epg.model.VideoComponent;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class VideoDialogView extends Stage{
    VBox videoPane;
    Scene videoScene;
    Label videoLabel;
    Label pathLabel;
    TextField videoCaption;
    Label heightLabel;
    Label widthLabel;
    HBox heightHBox;
    HBox widthHBox;
    TextField heightTextField;
    TextField widthTextField;
    HBox videoHBox;
    Button selectVideoButton;
    Button okButton;
    Button cancelButton;
    VideoSelectionController videoController;
    String path;
    Page page;
    VideoComponent videoComponent;
    
    public VideoDialogView(Stage primaryStage, Page initPage, boolean editPressed) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        page = initPage;
        videoComponent = new VideoComponent();
        if (editPressed)
            videoComponent = (VideoComponent)page.getSelectedComponent();
        videoCaption = new TextField();
        videoCaption.setText(videoComponent.getCaption());
        videoLabel = new Label(); 
        pathLabel = new Label(videoComponent.getContent());
        selectVideoButton = new Button("Select Video");
        path = videoComponent.getPath();
	updateVideo(path);
        heightLabel = new Label("Height: ");
        widthLabel = new Label(" Width: ");
        heightLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        widthLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        heightTextField = new TextField();
        widthTextField = new TextField();
        heightTextField.setText(videoComponent.getHeight());
        widthTextField.setText(videoComponent.getWidth());
        heightHBox = new HBox();
        widthHBox = new HBox();
        heightHBox.setAlignment(Pos.CENTER);
        widthHBox.setAlignment(Pos.CENTER);
        heightHBox.getChildren().addAll(heightLabel, heightTextField);
        widthHBox.getChildren().addAll(widthLabel, widthTextField);
        videoHBox = new HBox();
        videoHBox.getChildren().addAll(pathLabel, selectVideoButton);
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
        videoPane = new VBox();
        selectVideoButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        pathLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        videoHBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        videoLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        videoPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        videoPane.getChildren().addAll(videoLabel, videoHBox, videoCaption, heightHBox, widthHBox, buttonBox);
        initHandlers(editPressed);
        videoScene = new Scene(videoPane);
	videoScene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(videoScene);
    }
    public void show(String message) {
        videoLabel.setText(message);
        this.showAndWait();
    }
    public void updateVideo(String path) {
        File file = new File(path);
        path = file.getName();
	pathLabel.setText(path);
    }    
    public void initHandlers(boolean editPressed){
        videoController = new VideoSelectionController();
	selectVideoButton.setOnMousePressed(e -> {
	    path = videoController.processSelectVideo(path);
            updateVideo(path);
	});
        okButton.setOnMousePressed(e->{
            videoComponent.setVideoName(pathLabel.getText());
            videoComponent.setPath(path);
            videoComponent.setCaption(videoCaption.getText());
            videoComponent.setHeight(heightTextField.getText());
            videoComponent.setWidth(widthTextField.getText());
            if (!editPressed)
                page.addComponent(videoComponent);
            close();
        });
        cancelButton.setOnMousePressed(e->{
            close();
        });
        
    }
}

