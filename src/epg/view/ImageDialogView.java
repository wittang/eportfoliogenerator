/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.controller.ImageSelectionController;
import epg.error.ErrorHandler;
import epg.model.ImageComponent;
import epg.model.Page;
import java.io.File;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class ImageDialogView extends Stage{
    VBox imagePane;
    ComboBox alignmentChoice;
    Scene imageScene;
    Label imageLabel;
    TextField imageCaption;
    Label heightLabel;
    Label widthLabel;
    HBox heightHBox;
    HBox widthHBox;
    TextField heightTextField;
    TextField widthTextField;
    Button okButton;
    Button cancelButton;
    ImageView imageSelectionView;
    ImageSelectionController imageController;
    String path;
    Page page;
    ImageComponent imageComponent;
    
    public ImageDialogView(Stage primaryStage, Page initPage, boolean editPressed) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        page = initPage;
        imageComponent = new ImageComponent();
        if (editPressed)
            imageComponent = (ImageComponent)page.getSelectedComponent();
        imageCaption = new TextField();
        imageCaption.setText(imageComponent.getCaption());
        imageLabel = new Label(); 
        //image selection
        imageSelectionView = new ImageView();
        path = imageComponent.getPath();
	updateImage(path);
        alignmentChoice = new ComboBox();
        alignmentChoice.getItems().addAll("Left", "Right");
        alignmentChoice.getSelectionModel().select(imageComponent.getAlignment());
        heightLabel = new Label("Height: ");
        widthLabel = new Label(" Width: ");
        heightLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        widthLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        heightTextField = new TextField();
        widthTextField = new TextField();
        heightTextField.setText(imageComponent.getHeight());
        widthTextField.setText(imageComponent.getWidth());
        heightHBox = new HBox();
        widthHBox = new HBox();
        heightHBox.getChildren().addAll(heightLabel, heightTextField);
        widthHBox.getChildren().addAll(widthLabel, widthTextField);
        
        // YES and CANCEL BUTTONS
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
	       
        // WE'LL PUT EVERYTHING HERE
        imagePane = new VBox();
        imageLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        alignmentChoice.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        imagePane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        imagePane.getChildren().addAll(imageLabel, alignmentChoice, imageSelectionView, imageCaption, heightHBox, widthHBox, buttonBox);
        
        initHandlers(editPressed);
        // AND PUT IT IN THE WINDOW
        imageScene = new Scene(imagePane);
	imageScene.getStylesheets().add(STYLE_SHEET_UI);
        this.setScene(imageScene);
    }
    public void show(String message) {
        imageLabel.setText(message);
        this.showAndWait();
    }
    public void updateImage(String path) {
	File file = new File(path);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError();
	}
    }    
    public void initHandlers(boolean editPressed){
        imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	    path = imageController.processSelectImage(path);
            updateImage(path);
	});
        okButton.setOnMousePressed(e->{
            imageComponent.setPath(path);
            imageComponent.setCaption(imageCaption.getText());
            imageComponent.setHeight(heightTextField.getText());
            imageComponent.setWidth(widthTextField.getText());
            imageComponent.setAlignment(alignmentChoice.getValue().toString());
            if (!editPressed)
                page.addComponent(imageComponent);
            close();
        });
        cancelButton.setOnMousePressed(e->{
            close();
        });
        
    }
}

