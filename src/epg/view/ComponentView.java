/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_COMPONENT_VIEW;
import static epg.StartupConstants.CSS_CLASS_PAGE_VIEW;
import static epg.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import epg.model.Component;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Wilson
 */
public class ComponentView extends StackPane{
    PageEditView ui;
    
    // SLIDE THIS COMPONENT EDITS
    Component component;
    // CONTROLS FOR EDITING THE CAPTION
    Label componentType;
    public ComponentView(PageEditView initUi, Component initComponent) {
	// KEEP THIS FOR LATER
	ui = initUi;
	this.getStyleClass().add(CSS_CLASS_COMPONENT_VIEW);
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	//this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	component = initComponent;
	// SETUP THE CAPTION CONTROLS
	componentType = new Label(component.getComponentType() + " Component: " + component.getContent());

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(componentType);
    }
}
