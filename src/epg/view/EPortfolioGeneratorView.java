/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.*;
import epg.controller.EPortfolioController;
import epg.controller.FileController;
import epg.error.ErrorHandler;
import epg.file.EPortfolioExporter;
import epg.file.EPortfolioFileManager;
import epg.model.EPortfolio;
import epg.model.Page;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class EPortfolioGeneratorView {
    
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    VBox epgPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newEPortfolioButton;
    Button loadEPortfolioButton;
    Button saveEPortfolioButton;
    Button saveAsButton;
    Button exportEPortfolioButton;
    Button exitButton;
    
    // WORKSPACE
    HBox workspace;
    VBox ePortfolioWorkspace;
    VBox studentNameHBox;
    Label studentName;
    TextField studentNameTextField;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox siteToolbar;
    PageView ePortfolioView;
    Button addPageButton;
    Button removePageButton;
    
    // AND THIS WILL GO IN THE CENTER
    VBox pageWorkspace;
    ScrollPane pageScrollPane;
    VBox pagePane;
    
    VBox workspaceMode;
    SiteView siteView;
    FlowPane workspaceModeToolbar;
    Button pageEditorButton;
    Button siteViewerButton;
    PageEditView pageEditView;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    EPortfolio ePortfolio;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    EPortfolioFileManager fileManager;
    
    // THIS IS FOR EXPORTING THE SLIDESHOW SITE
    EPortfolioExporter siteExporter;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private EPortfolioController ePortfolioController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public EPortfolioGeneratorView(EPortfolioFileManager initFileManager,
				EPortfolioExporter initSiteExporter) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// AND THE SITE EXPORTER
	siteExporter = initSiteExporter;
	
	// MAKE THE DATA MANAGING MODEL
	ePortfolio = new EPortfolio(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public EPortfolio getEPortfolio() {
	return ePortfolio;
    }
    public PageEditView getPageEditView(){
        return pageEditView;
    }
    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace(); 

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
        
    }
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
        //ePortfolioWorkspace = new HBox();
        studentNameHBox = new VBox(20);
        studentNameHBox.getStyleClass().add(CSS_CLASS_PAGE_EDIT_HBOX);
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	siteToolbar = new VBox(20);
	addPageButton = this.initChildButton(siteToolbar, ICON_ADD, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
	removePageButton = this.initChildButton(siteToolbar, ICON_REMOVE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
	// AND THIS WILL GO IN THE CENTER
        studentName = new Label("Student Name: ");
        studentName.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        studentNameTextField = new TextField();
        studentNameTextField.setMaxWidth(150);
        studentNameHBox.getChildren().addAll(studentName, studentNameTextField);
        pageWorkspace = new VBox();
        pageWorkspace.setPadding(new Insets(20, 0, 0, 0));
	pagePane = new VBox(10);
	pageScrollPane = new ScrollPane(pagePane);
        pagePane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
        reloadPagePane();
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	//ePortfolioWorkspace.getChildren().addAll(siteToolbar, pageScrollPane);
        
        pageWorkspace.getChildren().addAll(studentNameHBox, pageScrollPane);
        workspace.getChildren().addAll(siteToolbar, pageWorkspace);
        
        

	// SETUP ALL THE STYLE CLASSES
        siteToolbar.getStyleClass().add(CSS_CLASS_VERTICAL_TOOLBAR_PANE);
	pageScrollPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
	workspace.getStyleClass().add(CSS_CLASS_WORKSPACE);
    }
    private void initEventHandlers() {
        
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager, siteExporter);
	newEPortfolioButton.setOnAction(e -> {
	    fileController.handleNewEPortfolioRequest();
            if (workspace.getChildren().size()>2)
                workspace.getChildren().remove(2);
	});
	loadEPortfolioButton.setOnAction(e -> {
	    fileController.handleLoadEPortfolioRequest();
            if (workspace.getChildren().size()>2)
                workspace.getChildren().remove(2);
	});
	saveEPortfolioButton.setOnAction(e -> {
	    fileController.handleSaveEPortfolioRequest(ePortfolio);
	});
        saveAsButton.setOnAction(e -> {
            fileController.handleSaveAsEPortfolioRequest();
        });
	exportEPortfolioButton.setOnAction(e -> {
	    fileController.handleExportEPortfolioRequest();
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	ePortfolioController = new EPortfolioController(this);
	addPageButton.setOnAction(e -> {
	    ePortfolioController.processAddPageRequest();
	});
	removePageButton.setOnAction(e -> {
	    ePortfolioController.processRemovePageRequest();
            workspace.getChildren().remove(2);
	});
    }
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
	fileToolbarPane.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	newEPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadEPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	saveAsButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exportEPortfolioButton = initChildButton(fileToolbarPane, ICON_EXPORT_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }
    public void initWorkspaceMode(Page page){
        workspaceMode = new VBox();
        workspaceMode.getStyleClass().add(CSS_CLASS_WORK_SPACE_MODE);
        workspaceModeToolbar = new FlowPane();
        workspaceModeToolbar.setAlignment(Pos.CENTER);
        workspaceModeToolbar.setPadding(new Insets(10, 0, 20, 0));
        //workspaceModeToolbar.getStyleClass().add(CSS_CLASS_PAGE_EDIT_HBOX);
        pageEditorButton = new Button("Page Editor");
        siteViewerButton = new Button("Site Viewer");
        pageEditorButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        siteViewerButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        workspaceModeToolbar.setHgap(75);
        workspaceModeToolbar.getChildren().addAll(pageEditorButton, siteViewerButton);
        
        pageEditView = new PageEditView(this, page);
        
        siteView = new SiteView(this);
        
        workspaceMode.getChildren().addAll(workspaceModeToolbar, pageEditView);
        workspaceMode.setVgrow(pageEditView, Priority.ALWAYS);
        pageEditorButton.setOnAction(e->{
            workspaceMode.getChildren().remove(1);
            workspaceMode.getChildren().add(pageEditView);
        });
        siteViewerButton.setOnAction(e->{
            workspaceMode.getChildren().remove(1);
            workspaceMode.getChildren().add(siteView);
        });
    }
    
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	epgPane = new VBox();
	//epgPane.getStyleClass().add(CSS_CLASS_WORKSPACE);
	epgPane.getChildren().add(fileToolbarPane);
	primaryScene = new Scene(epgPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        epgPane.getStyleClass().add(CSS_CLASS_WHOLESPACE);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath) {};
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
    public void reloadPagePane() {
        System.out.println(ePortfolio.getStudentName());
        studentNameTextField.setText(ePortfolio.getStudentName());
	pagePane.getChildren().clear();
	for (Page page : ePortfolio.getPages()) {
	    PageView ePortfolioPage = new PageView(this, page);
	    if (ePortfolio.isSelectedPage(page))
		ePortfolioPage.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
	    else
		ePortfolioPage.getStyleClass().add(CSS_CLASS_PAGE_VIEW);
	    pagePane.getChildren().add(ePortfolioPage);
	    ePortfolioPage.setOnMousePressed(e -> {
		ePortfolio.setSelectedPage(page);
                initWorkspaceMode(page);
                if (workspace.getChildren().size()>2)
                    workspace.getChildren().remove(2);
                workspace.getChildren().add(workspaceMode);
                workspace.setHgrow(workspaceMode, Priority.ALWAYS);
		this.reloadPagePane();
	    });
	}
	updateSiteToolbarControls();
    }
    
    public void updateSiteToolbarControls() {
	// AND THE SLIDESHOW EDIT TOOLBAR
	addPageButton.setDisable(false);
	boolean pageSelected = ePortfolio.isPageSelected();
	removePageButton.setDisable(!pageSelected);	
    }
    public void updateFileToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
        if (epgPane.getChildren().size()<2){
            epgPane.getChildren().add(workspace);
            epgPane.setVgrow(workspace, Priority.ALWAYS);
            studentNameTextField.textProperty().addListener(e -> {
                ePortfolio.setStudentName(studentNameTextField.getText());
                updateFileToolbarControls(false);
	});
        }
        
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveEPortfolioButton.setDisable(saved);
        saveAsButton.setDisable(false);
	exportEPortfolioButton.setDisable(false);
	
	updateSiteToolbarControls();
    }
}
