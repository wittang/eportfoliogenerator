/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;
import static epg.StartupConstants.CSS_CLASS_LANG_COMBO_BOX;
import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.HeaderComponent;
import epg.model.HyperlinkComponent;
import epg.model.Page;
import epg.model.ParagraphComponent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class TextDialogView extends Stage{
    VBox textPane;
    ComboBox textChoice;
    Scene textScene;
    Label textLabel;
    TextField textField;
    TextArea textbox;
    Button okButton;
    Button cancelButton;
    String textSelection;
    ComboBox fontComboBox;
    Page page;
    String content;
    HBox paragraphHBox;
    HyperlinkList hyperlinks;
    Stage stage;
    HyperlinkDialogView hyperlinkDialog;
    ParagraphComponent paragraph;
    HeaderComponent header;
    ListDialogView listDialogView;
    public TextDialogView(Stage primaryStage, Page initPage, boolean editPressed) {
        stage = primaryStage;
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        page = initPage;
    
        if (editPressed == true){
            String componentType = page.getSelectedComponent().getComponentType();
            switch(componentType){
                case "Header": 
                    initHeader(editPressed);
                    break;
                case "Paragraph":
                    initParagraph(editPressed);
                    break;
                case "List":
                    initList(page, editPressed);
                    break;
            }
        }
        else {
        textLabel = new Label(); 
        textChoice = new ComboBox();
        textChoice.getItems().addAll("Header", "Paragraph", "List");
        textChoice.getSelectionModel().select("Header");
        textSelection = textChoice.getValue().toString();
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        
        textChoice.setOnAction(e->{
            textSelection = textChoice.getValue().toString();
        });
        okButton.setOnAction(e->{
            if (textSelection.equals("Header"))
                initHeader(editPressed);
            if (textSelection.equals("Paragraph"))
                initParagraph(editPressed);
            if (textSelection.equals("List"))
                initList(page, editPressed);
        });
        cancelButton.setOnAction(e->close());
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
        textPane = new VBox();
        textLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        textChoice.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getChildren().addAll(textLabel, textChoice, buttonBox);
	okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        textScene = new Scene(textPane);
	textScene.getStylesheets().add(STYLE_SHEET_UI);
        
        this.setScene(textScene);
        }
    }
    public void initHeader(boolean editPressed){
        textSelection = "Header";
        header = new HeaderComponent();
        if (editPressed)
            header = (HeaderComponent)page.getSelectedComponent();
        textLabel = new Label("Header");
        textField = new TextField();
        textField.setText(header.getContent());
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        initButtonHandler(editPressed);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
        textPane = new VBox();
        textLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getChildren().addAll(textLabel, textField, buttonBox);
        if (!editPressed){
            textScene.setRoot(textPane);
            textScene.getWindow().sizeToScene();
        }
        else {
            textScene = new Scene(textPane);
            textScene.getStylesheets().add(STYLE_SHEET_UI);
        }
        this.setScene(textScene);
    }
    
    public void initParagraph(boolean editPressed){
        textSelection = "Paragraph";
        paragraph = new ParagraphComponent();
        if (editPressed)
            paragraph = (ParagraphComponent)page.getSelectedComponent();
        textLabel = new Label("Paragraph");
        fontComboBox = new ComboBox();
        Button addHyperlinkButton = new Button("Add Hyperlink");
        HBox hyperlinkFont = new HBox();
        fontComboBox.getItems().addAll("Font 1", "Font 2", "Font 3", "Font 4", "Font 5");
        fontComboBox.getSelectionModel().select(paragraph.getFont());
        hyperlinkFont.getChildren().addAll(fontComboBox, addHyperlinkButton);
        addHyperlinkButton.setDisable(true);
        textbox = new TextArea();
        textbox.setText(paragraph.getContent());
        hyperlinks = new HyperlinkList(this, paragraph);
        paragraphHBox = new HBox();
        paragraphHBox.getChildren().addAll(textbox, hyperlinks);
        paragraphHBox.setHgrow(hyperlinks, Priority.ALWAYS);
        textbox.setOnDragDetected(e->{
            if(!textbox.getSelectedText().isEmpty())
                addHyperlinkButton.setDisable(false);
        });
        addHyperlinkButton.setOnMouseClicked(e->{
            if (!textbox.getSelectedText().isEmpty()){
                hyperlinkDialog = new HyperlinkDialogView(stage, "");
                HyperlinkComponent hyperlink = new HyperlinkComponent(textbox.getSelectedText(), 
                    hyperlinkDialog.hyperlink, 
                    textbox.getSelection().getStart(), 
                    textbox.getSelection().getEnd(), 0);
                if (hyperlinkDialog.choice.equals("add"))
                    paragraph.getHyperlinks().add(hyperlink);
                System.out.println(hyperlink.getText() + " " + hyperlink.getUrl() + " " +hyperlink.getStart() + "-" +hyperlink.getEnd());
                hyperlinks.reloadItemsPane();
            }
            addHyperlinkButton.setDisable(true);
        });
        okButton = new Button("OK");
        cancelButton = new Button("CANCEL");
        initButtonHandler(editPressed);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton, cancelButton);
        textPane = new VBox();
        addHyperlinkButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        hyperlinkFont.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        fontComboBox.getStyleClass().add(CSS_CLASS_LANG_COMBO_BOX);
        okButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
	cancelButton.getStyleClass().add(CSS_CLASS_LANG_OK_BUTTON);
        buttonBox.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getStyleClass().add(CSS_CLASS_LANG_DIALOG_PANE);
        textPane.getChildren().addAll(textLabel, hyperlinkFont, paragraphHBox, hyperlinks, buttonBox);
        if (!editPressed){
            textScene.setRoot(textPane);
            textScene.getWindow().sizeToScene();
        }
        else {
            textScene = new Scene(textPane);
            textScene.getStylesheets().add(STYLE_SHEET_UI);
        }
        this.setScene(textScene);
    }
    
    public void initList(Page page, boolean editPressed){
        if (editPressed == true){
            textLabel = new Label();
            textPane = new VBox();
            textScene = new Scene(textPane);
            textScene.getStylesheets().add(STYLE_SHEET_UI);
            this.setScene(textScene);
        }
        listDialogView = new ListDialogView(textScene, page, editPressed);
        textScene.getWindow().sizeToScene();
    }
    
    public void show(String message) {
        textLabel.setText(message);
        this.showAndWait();
        textScene.getWindow().sizeToScene();
    }
    
    public void initButtonHandler(boolean editPressed){
        okButton.setOnMousePressed(e->{
            if (textSelection.equals("Paragraph")){
                paragraph.setContent(textbox.getText());
                paragraph.setFont(fontComboBox.getValue().toString());
                if (!editPressed)
                    page.addComponent(paragraph);
            }
            else{
                header.setContent(textField.getText());
                if (!editPressed)
                    page.addComponent(header);
            }
            close();
        });
        cancelButton.setOnMousePressed(e->{
            close();
        });
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath) {};
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
}
