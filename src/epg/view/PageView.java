/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;
import static epg.StartupConstants.CSS_CLASS_PAGE_VIEW;
import static epg.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import epg.model.Page;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Wilson
 */
public class PageView extends StackPane{
    EPortfolioGeneratorView ui;
    
    // SLIDE THIS COMPONENT EDITS
    Page page;
    // CONTROLS FOR EDITING THE CAPTION
    Label pageTitle;
    public PageView(EPortfolioGeneratorView initUi, Page initPage) {
	// KEEP THIS FOR LATER
	ui = initUi;
	this.getStyleClass().add(CSS_CLASS_PAGE_VIEW);
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	//this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	page = initPage;
	// SETUP THE CAPTION CONTROLS
	pageTitle = new Label(page.getTitle());

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(pageTitle);
    }
}
