/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import static epg.StartupConstants.CSS_CLASS_LANG_DIALOG_PANE;
import static epg.StartupConstants.CSS_CLASS_LANG_OK_BUTTON;
import static epg.StartupConstants.CSS_CLASS_LANG_PROMPT;
import static epg.StartupConstants.CSS_CLASS_PAGES;
import static epg.StartupConstants.CSS_CLASS_PAGE_EDIT_VBOX;
import static epg.StartupConstants.CSS_CLASS_SCROLLPANE;
import static epg.StartupConstants.CSS_CLASS_SELECTED_ITEM;
import static epg.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_ADD;
import static epg.StartupConstants.ICON_EDIT;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import epg.model.Component;
import epg.model.HyperlinkComponent;
import epg.model.ListComponent;
import epg.model.Page;
import epg.model.ParagraphComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Wilson
 */
public class HyperlinkList extends VBox{
    Label hyperlinkLabel;
    Label hyperlink;
    ScrollPane hyperlinkScrollPane;
    VBox hyperlinkVBox;
    HBox hyperlinkHBox;
    Button removeItemButton;
    Button editItemButton;
    //ObservableList<Label> hyperlinks;
    int selectedItemIndex;
    String type;
    Component component;
    Stage stage;
    ObservableList<HyperlinkComponent> hyperlinkList;
    
    public HyperlinkList(Stage initStage, Component componentToEdit) {
        //this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VBOX);
        stage = initStage;
        component = componentToEdit;
        type = component.getComponentType();
        if (type.equals("Paragraph"))
            hyperlinkList = ((ParagraphComponent)componentToEdit).getHyperlinks();
        else
            hyperlinkList = ((ListComponent)componentToEdit).getHyperlinks();
        //hyperlinks = FXCollections.observableArrayList();
        hyperlinkLabel = new Label("Hyperlinks");
        hyperlinkVBox = new VBox();
        hyperlinkVBox.setPadding(new Insets(0, 20, 0, 20));
        hyperlinkVBox.setMaxWidth(Double.MAX_VALUE);
        hyperlinkVBox.setAlignment(Pos.CENTER);
        hyperlinkScrollPane = new ScrollPane(hyperlinkVBox);
        hyperlinkScrollPane.setFitToHeight(true);
        hyperlinkScrollPane.setFitToWidth(true);
        VBox removeEditToolbar = new VBox();
        //hyperlinkScrollPane.getStyleClass().add(CSS_CLASS_PAGES);
        removeItemButton = initChildButton(removeEditToolbar, ICON_REMOVE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        editItemButton = initChildButton(removeEditToolbar, ICON_EDIT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        initHandlers();
        hyperlinkHBox = new HBox();
        hyperlinkHBox.getChildren().addAll(hyperlinkScrollPane, removeEditToolbar);
        hyperlinkHBox.setHgrow(hyperlinkScrollPane, Priority.ALWAYS);
        hyperlinkLabel.getStyleClass().add(CSS_CLASS_LANG_PROMPT);
        getChildren().addAll(hyperlinkLabel, hyperlinkHBox);
        //setVgrow(hyperlinkHBox, Priority.ALWAYS);
        getStyleClass().add(CSS_CLASS_PAGE_EDIT_VBOX);
        hyperlinkScrollPane.getStyleClass().add(CSS_CLASS_SCROLLPANE);
        /*if (editPressed){
            listLabel.setText("Edit List");
            tempList = (ListComponent)page.getSelectedComponent();
            listVBox.getChildren().clear();
            for (String items : tempList.getItems()){
                item = new HBox();
                listVBox.getChildren().add(item);
            }
        }*/
        selectedItemIndex = -1;
        reloadItemsPane();
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath) {};
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
    
    public void initHandlers(){
        removeItemButton.setOnMouseClicked(e->{
            if (type.equals("Paragraph"))
                ((ParagraphComponent)component).getHyperlinks().remove(selectedItemIndex);
            else 
                ((ListComponent)component).getHyperlinks().remove(selectedItemIndex);
            selectedItemIndex = -1;
            reloadItemsPane();
        });
        editItemButton.setOnMouseClicked(e->{
            System.out.println(selectedItemIndex+ ": selected item is named " + hyperlinkList.get(selectedItemIndex).getText());
            String url = hyperlinkList.get(selectedItemIndex).getUrl();
            HyperlinkDialogView hyperlinkDialog = new HyperlinkDialogView(stage, url);
            if (hyperlinkDialog.choice.equals("add"))
                hyperlinkList.get(selectedItemIndex).setUrl(url);
        });
    }
    public void reloadItemsPane(){
        hyperlinkVBox.getChildren().clear();
        if (selectedItemIndex < 0){
            removeItemButton.setDisable(true);
            editItemButton.setDisable(true);
        }
	for (HyperlinkComponent item : hyperlinkList) {
            int index = hyperlinkList.indexOf(item);
            System.out.println("hyperlink " +index+ " is being loaded onto list");
            String url = item.getText();
            Label hyperlink = new Label(url);
            if (selectedItemIndex == index){
                hyperlink.getStyleClass().add(CSS_CLASS_SELECTED_ITEM);
            }
            else{
                hyperlink.getStyleClass().remove(CSS_CLASS_SELECTED_ITEM);
            }
            hyperlinkVBox.getChildren().add(hyperlink);
            hyperlink.setOnMousePressed(e->{
                removeItemButton.setDisable(false);
                editItemButton.setDisable(false);
                selectedItemIndex = index;
                reloadItemsPane();
            });
        }
    } 
}
