/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Wilson
 */
public class HeaderComponent extends Component{
    static String type = "Header";
    String content;
    public HeaderComponent(){
        super();
        super.setType(type);
        content = "";
    }
    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content = content;
    }
}
