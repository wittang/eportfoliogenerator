/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Wilson
 */
public class ParagraphComponent extends Component{
    static String type = "Paragraph";
    String content;
    ObservableList <HyperlinkComponent> hyperlinks;
    String font;
    public ParagraphComponent(){
        super();
        super.setType(type);
        hyperlinks = FXCollections.observableArrayList();
        content = "";
        font = "Font 1";
    }
    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content = content;
    }
    public String getFont(){
        return font;
    }
    public void setFont(String font)
    {
        this.font = font; 
    }
    public ObservableList<HyperlinkComponent> getHyperlinks() {
        return hyperlinks;
    }
          
}
