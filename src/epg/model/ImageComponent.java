/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import static epg.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static epg.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import java.io.File;

/**
 *
 * @author Wilson
 */
public class ImageComponent extends Component{
    static String type = "Image";
    String content;
    String imageName;
    String path;
    String caption;
    String height;
    String width;
    String alignment;
    public ImageComponent(){
        super();
        super.setType(type);
        imageName = DEFAULT_SLIDE_IMAGE;
        path = PATH_SLIDE_SHOW_IMAGES + DEFAULT_SLIDE_IMAGE;
        caption = "Enter Caption";
        height = "";
        width = "";
        alignment = "Left";
    }
    public String getContent(){
        return imageName;
    }
    public String getPath(){
        return path;
    }
    public void setImageName(String imageName){
        this.imageName = imageName;
    }
    public void setPath(String path){
         this.path = path;
    }
    public String getCaption(){
        return caption;
    }
    public void setCaption(String caption){
        this.caption = caption;
    }
    
    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getAlignment() {
        return alignment;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }
    
    
    
    
    
}

