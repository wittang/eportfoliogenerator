/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import epg.view.EPortfolioGeneratorView;

/**
 *
 * @author Wilson
 */
public class EPortfolio {
    EPortfolioGeneratorView ui;
    String name;
    ObservableList<Page> pages;
    Page selectedPage;
    String studentName;
    
    public EPortfolio(EPortfolioGeneratorView initUI) {
	ui = initUI;
	pages = FXCollections.observableArrayList();
	reset();
    }

    // ACCESSOR METHODS
    public boolean isPageSelected() {
	return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage) {
	return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
    public Page getSelectedPage() {
	return selectedPage;
    }

    public String getName() { 
	return name; 
    }
    
    // MUTATOR METHODS
    public void setSelectedPage(Page initSelectedPage) {
	selectedPage = initSelectedPage;
    }
    
    public void setName(String initName) { 
	name = initName; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	pages.clear();
        name = "";
        studentName = "";
	selectedPage = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption Caption for the slide image to add.
     */
    public void addPage() {
	Page pageToAdd = new Page(ui);
	pages.add(pageToAdd);
	ui.reloadPagePane();
    }
    public void removeSelectedPage() {
	if (isPageSelected()) {
	    pages.remove(selectedPage);
	    selectedPage = null;
	    ui.reloadPagePane();
	}
    }
    public String getStudentName(){
        return studentName;
    }
    public void setStudentName(String studentName){
        this.studentName = studentName;
    }
    public EPortfolioGeneratorView getUI(){
        return ui;
    }
    
}
