/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.io.File;

/**
 *
 * @author Wilson
 */
public class VideoComponent extends Component{
    static String type = "Video";
    String content;
    String videoName;
    String path;
    String caption;
    String height;
    String width;
    public VideoComponent(){
        super();
        super.setType(type);
        videoName = "No Video Selected";
        path = "No Video Selected";
        caption = "Enter Caption";
        height = "";
        width = "";
    }
    public String getContent(){
        return videoName;
    }
    public void setVideoName(String videoName){
        this.videoName = videoName;
    }
    public String getPath(){
        return path;
    }
    public void setPath(String path){
         this.path = path;
    }
    public String getCaption(){
        return caption;
    }
    public void setCaption(String caption){
        this.caption = caption;
    }
    
    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
