/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

/**
 *
 * @author Wilson
 */
public class HyperlinkComponent {
    String text;
    String url;
    int start;
    int end;
    int itemNo;
    public HyperlinkComponent(String text, String url, int start, int end, int itemNo){
        this.text = text;
        this.url = url;
        this.start = start;
        this.end = end;
        this.itemNo = itemNo;
    }
    public String getText(){
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
    public int getItemNo(){
        return itemNo;
    }
    public void setItemNo(int itemNo){
        this.itemNo = itemNo;
    }
            
}
