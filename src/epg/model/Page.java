/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import static epg.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static epg.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import epg.view.EPortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Wilson
 */
public class Page {
    EPortfolioGeneratorView ui;
    String title;
    String footer;
    String bannerImageName;
    String bannerImagePath;
    String layout;
    String color;
    String font;
    ObservableList<Component> components;
    Component selectedComponent;

    public Page(EPortfolioGeneratorView initUI){
        this.ui = initUI;
        components = FXCollections.observableArrayList();
        bannerImageName = DEFAULT_SLIDE_IMAGE;
        bannerImagePath = PATH_SLIDE_SHOW_IMAGES + DEFAULT_SLIDE_IMAGE;
        title = "Page Title";
        footer = "";
        layout = "Layout 1";
        color = "Sky Blue/Grey";
        font = "Bitter";
        reset();
        
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public ObservableList<Component> getComponents() {
        return components;
    }

    public void setComponents(ObservableList<Component> components) {
        this.components = components;
    }
    public String getBannerImagePath() {
        return bannerImagePath;
    }
    public void setBannerImagePath(String bannerImagePath) {
        this.bannerImagePath = bannerImagePath;
    }
    public boolean isComponentSelected() {
	return selectedComponent != null;
    }
    
    public boolean isSelectedComponent(Component testComponent) {
	return selectedComponent == testComponent;
    }
    public Component getSelectedComponent() {
	return selectedComponent;
    }
    public void setSelectedComponent(Component initSelectedComponent) {
	selectedComponent = initSelectedComponent;
    }
    public String getBannerImageName(){
        return bannerImageName;
    }
    public void setBannerImageName(String imageName){
        bannerImageName = imageName;
    }
    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }
    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	components.clear();
	selectedComponent = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption Caption for the slide image to add.
     */
    public void addComponent(Component componentToAdd) {
        components.add(componentToAdd);
	ui.getPageEditView().reloadComponentsPane();
    }
    public void removeSelectedComponent() {
	if (isComponentSelected()) {
	    components.remove(selectedComponent);
	    selectedComponent = null;
	    ui.reloadPagePane();
	}
    }
}
