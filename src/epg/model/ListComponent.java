/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Wilson
 */
public class ListComponent extends Component{
    static String type = "List";
    ObservableList<String> items;
    String selectedItem;
    ObservableList <HyperlinkComponent> hyperlinks;
    public ListComponent(){
        super();
        super.setType(type);
        items = FXCollections.observableArrayList();
        hyperlinks = FXCollections.observableArrayList();
    }
    public ObservableList<String> getItems(){
        return items;
    }
    public String getContent(){
        return "Item 1: " + items.get(0);
    }
    public ObservableList<HyperlinkComponent> getHyperlinks() {
        return hyperlinks;
    }
    
}
