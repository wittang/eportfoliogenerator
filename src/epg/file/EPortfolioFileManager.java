/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.StartupConstants.PATH_EPORTFOLIOS;
import static epg.file.EPortfolioExporter.DATA_DIR;
import static epg.file.EPortfolioExporter.DATA_FILE;
import static epg.file.EPortfolioExporter.SITES_DIR;
import static epg.file.EPortfolioExporter.SLASH;
import epg.model.Component;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.*;
import javax.json.stream.JsonGenerator;
import epg.model.EPortfolio;
import epg.model.HeaderComponent;
import epg.model.HyperlinkComponent;
import epg.model.ImageComponent;
import epg.model.ListComponent;
import epg.model.Page;
import epg.model.ParagraphComponent;
import epg.model.Slide;
import epg.model.SlideshowComponent;
import epg.model.VideoComponent;
/**
 *
 * @author Wilson
 */
public class EPortfolioFileManager {
    

    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @param slideShowToSave The course whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveEPortfolio(EPortfolio ePortfolio, boolean savePressed) throws IOException {
	StringWriter sw = new StringWriter();

	// BUILD THE SLIDES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(ePortfolio.getPages());

	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject ePortfolioJsonObject = Json.createObjectBuilder()
		.add("name", ePortfolio.getName())
		.add("student_name", ePortfolio.getStudentName())
                .add("pages", pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(ePortfolioJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String ePortfolioName = "" + ePortfolio.getName();
        String jsonFilePath = "";
        if (savePressed)
            jsonFilePath = PATH_EPORTFOLIOS + "/" + ePortfolioName + ".json";
        else
            jsonFilePath = SITES_DIR + ePortfolioName + SLASH + DATA_DIR + DATA_FILE;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(ePortfolioJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel object.
     *
     * @param slideShowToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
    public void loadEPortfolio(EPortfolio ePortfolioToLoad, String jsonFilePath) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);

	ePortfolioToLoad.reset();
	ePortfolioToLoad.setName(json.getString("name"));
        ePortfolioToLoad.setStudentName(json.getString("student_name"));
	JsonArray jsonPagesArray = json.getJsonArray("pages");
	for (int i = 0; i < jsonPagesArray.size(); i++) {
	    JsonObject pageJso = jsonPagesArray.getJsonObject(i);
            Page page = new Page(ePortfolioToLoad.getUI());
            page.setTitle(pageJso.getString("title"));
            page.setLayout(pageJso.getString("layout"));
            page.setColor(pageJso.getString("color"));
            page.setFont(pageJso.getString("font"));
            page.setBannerImageName(pageJso.getString("banner_image_name"));
            page.setBannerImagePath(pageJso.getString("banner_image_path"));
            page.setFooter(pageJso.getString("footer"));
            JsonArray jsonComponentsArray = pageJso.getJsonArray("components");
            for (int j = 0; j < jsonComponentsArray.size(); j++){
                JsonObject componentJso = jsonComponentsArray.getJsonObject(j);
                String componentType = componentJso.getString("type");
                if (componentType.equals("Header")){
                    HeaderComponent header = new HeaderComponent();
                    header.setContent(componentJso.getString("content"));
                    page.getComponents().add(header);
                }
                if (componentType.equals("Paragraph")){
                    ParagraphComponent paragraph = new ParagraphComponent();
                    paragraph.setContent(componentJso.getString("content"));
                    paragraph.setFont(componentJso.getString("font"));
                    JsonArray jsonHyperlinksArray = componentJso.getJsonArray("hyperlinks");
                    for(int k = 0; k < jsonHyperlinksArray.size(); k++ ){
                        JsonObject hyperlinkJso = jsonHyperlinksArray.getJsonObject(k);
                        HyperlinkComponent hyperlink = new HyperlinkComponent(hyperlinkJso.getString("text"),
                                                            hyperlinkJso.getString("text"),
                                                            hyperlinkJso.getInt("start"),
                                                            hyperlinkJso.getInt("end"),
                                                            hyperlinkJso.getInt("itemNo"));
                        paragraph.getHyperlinks().add(hyperlink);
                    }
                    page.getComponents().add(paragraph);
                }
                if (componentType.equals("List")){
                    ListComponent list = new ListComponent();
                    JsonArray jsonItemsArray = componentJso.getJsonArray("items");
                    for(int k = 0; k < jsonItemsArray.size(); k++ ){
                        JsonObject itemJso = jsonItemsArray.getJsonObject(k);
                        list.getItems().add(itemJso.getString("item"));
                    }
                    JsonArray jsonHyperlinksArray = componentJso.getJsonArray("hyperlinks");
                    for(int k = 0; k < jsonHyperlinksArray.size(); k++ ){
                        JsonObject hyperlinkJso = jsonHyperlinksArray.getJsonObject(k);
                        HyperlinkComponent hyperlink = new HyperlinkComponent(hyperlinkJso.getString("text"),
                                                            hyperlinkJso.getString("text"),
                                                            hyperlinkJso.getInt("start"),
                                                            hyperlinkJso.getInt("end"),
                                                            hyperlinkJso.getInt("itemNo"));
                        list.getHyperlinks().add(hyperlink);
                    }
                    page.getComponents().add(list);
                }
                if (componentType.equals("Image")){
                    ImageComponent image = new ImageComponent();
                    image.setImageName(componentJso.getString("image_name"));
                    image.setPath(componentJso.getString("image_path"));
                    image.setCaption(componentJso.getString("caption"));
                    image.setHeight(componentJso.getString("height"));
                    image.setWidth(componentJso.getString("width"));
                    image.setAlignment(componentJso.getString("alignment"));
                    page.getComponents().add(image);
                }
                if (componentType.equals("Video")){
                    VideoComponent video = new VideoComponent();
                    video.setVideoName(componentJso.getString("video_name"));
                    video.setPath(componentJso.getString("image_path"));
                    video.setCaption(componentJso.getString("caption"));
                    video.setHeight(componentJso.getString("height"));
                    video.setWidth(componentJso.getString("width"));
                    page.getComponents().add(video);
                }
                if (componentType.equals("Slideshow")){
                    SlideshowComponent slideshow = new SlideshowComponent();
                    slideshow.setTitle(componentJso.getString("title"));
                    JsonArray jsonSlidesArray = componentJso.getJsonArray("slides");
                    for (int k = 0; k < jsonSlidesArray.size(); k++) {
                        JsonObject slideJso = jsonSlidesArray.getJsonObject(k);
                        System.out.println(slideJso.getString("image_file_name") + slideJso.getString("image_path") + slideJso.getString("caption"));
                        slideshow.addSlide(slideJso.getString("image_file_name"),
                        slideJso.getString("image_path"),
                        slideJso.getString("caption"));
                    }
                    page.getComponents().add(slideshow);
                }
            }
            ePortfolioToLoad.getPages().add(page);
        }
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);
	ArrayList<String> items = new ArrayList();
	JsonArray jsonArray = json.getJsonArray(arrayName);
	for (JsonValue jsV : jsonArray) {
	    items.add(jsV.toString());
	}
	return items;
    }
    
    private JsonArray makePagesJsonArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
            for (Page page : pages) {
                JsonObject jso = makePageJsonObject(page);
                jsb.add(jso);
            }
            JsonArray jA = jsb.build();
            return jA;
    }
    private JsonObject makePageJsonObject(Page page){
        JsonArray componentsJsonArray = makeComponentsJsonArray(page.getComponents());
        JsonObject jso = Json.createObjectBuilder()
		.add("title", page.getTitle())
                .add("layout", page.getLayout())
                .add("color", page.getColor())
                .add("font", page.getFont())
                .add("banner_image_name", page.getBannerImageName())
                .add("banner_image_path", page.getBannerImagePath())
                .add("footer", page.getFooter())
                .add("components", componentsJsonArray)
		.build();
	return jso;
    }
    private JsonArray makeComponentsJsonArray(List<Component> components){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
            for (Component component : components) {
                JsonObject jso = null;
                switch(component.getComponentType()){
                    case "Header": jso = makeHeaderJsonObject((HeaderComponent)component); break;
                    case "Paragraph": jso = makeParagraphJsonObject((ParagraphComponent)component); break;
                    case "List": jso = makeListJsonObject((ListComponent)component); break;
                    case "Image": jso = makeImageJsonObject((ImageComponent)component); break;
                    case "Video": jso = makeVideoJsonObject((VideoComponent)component); break;
                    case "Slideshow": jso = makeSlideshowJsonObject((SlideshowComponent)component); break;
                }
                jsb.add(jso);
            }
            JsonArray jA = jsb.build();
            return jA;
    }
    private JsonObject makeHeaderJsonObject(HeaderComponent header){
        JsonObject jso = Json.createObjectBuilder()
		.add("type", "Header")
		.add("content", header.getContent())
		.build();
	return jso;
    }
    private JsonObject makeParagraphJsonObject(ParagraphComponent paragraph){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (HyperlinkComponent hyperlink : paragraph.getHyperlinks()) {
	    JsonObject jso2 = Json.createObjectBuilder()
                    .add("text", hyperlink.getText())
                    .add("url", hyperlink.getUrl())
                    .add("start", hyperlink.getStart())
                    .add("end", hyperlink.getEnd())
                    .add("itemNo", hyperlink.getItemNo())
                    .build();
	    jsb.add(jso2);
	}
        JsonArray hyperlinksJsonArray = jsb.build();
        JsonObject jso = Json.createObjectBuilder()
		.add("type", "Paragraph")
		.add("content", paragraph.getContent())
                .add("font", paragraph.getFont())
                .add("hyperlinks", hyperlinksJsonArray)
		.build();
	return jso;
    }
    private JsonObject makeListJsonObject(ListComponent list){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (HyperlinkComponent hyperlink : list.getHyperlinks()) {
	    JsonObject jso2 = Json.createObjectBuilder()
                    .add("text", hyperlink.getText())
                    .add("url", hyperlink.getUrl())
                    .add("start", hyperlink.getStart())
                    .add("end", hyperlink.getEnd())
                    .add("itemNo", hyperlink.getItemNo())
                    .build();
	    jsb.add(jso2);
	}
        JsonArray hyperlinksJsonArray = jsb.build();
        JsonArrayBuilder jsb2 = Json.createArrayBuilder();
	for (String item : list.getItems()) {
	    JsonObject jso2 = Json.createObjectBuilder().add("item", item).build();
	    jsb2.add(jso2);
	}
	JsonArray listJsonArray = jsb2.build();
        JsonObject jso = Json.createObjectBuilder()
		.add("type", "List")
                .add("items", listJsonArray)
                .add("hyperlinks", hyperlinksJsonArray)
		.build();
        return jso;
    }
    private JsonObject makeImageJsonObject(ImageComponent image){
        JsonObject jso = Json.createObjectBuilder()
		.add("type", "Image")
                .add("image_name", image.getContent())
		.add("image_path", image.getPath())
                .add("caption", image.getCaption())
                .add("height", image.getHeight())
                .add("width", image.getWidth())
                .add("alignment", image.getAlignment())
		.build();
	return jso;
    }
    private JsonObject makeVideoJsonObject(VideoComponent video){
        JsonObject jso = Json.createObjectBuilder()
		.add("type", "Video")
                .add("video_name", video.getContent())
		.add("image_path", video.getPath())
                .add("caption", video.getCaption())
                .add("height", video.getHeight())
                .add("width", video.getWidth())
		.build();
	return jso;
    }
    private JsonObject makeSlideshowJsonObject(SlideshowComponent slideshow){
        JsonArray slidesJsonArray = makeSlidesJsonArray(slideshow.getSlides());
        JsonObject jso = Json.createObjectBuilder()
                .add("type", "Slideshow")
		.add("title", slideshow.getTitle())
		.add("slides", slidesJsonArray)
		.build();
        return jso;
    }
    
    

    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }

    private JsonObject makeSlideJsonObject(Slide slide) {
	JsonObject jso = Json.createObjectBuilder()
		.add("image_file_name", slide.getImageFileName())
		.add("image_path", slide.getImagePath())
		.add("caption", slide.getCaption())
		.build();
	return jso;
    } 
    
}
